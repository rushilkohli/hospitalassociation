package hospital;

/**
 *
 * @author rushil
 */
public class Patients {
    private String name;
    private String dateOfBirth;
    private String familyDoctor;

    public String getName() {
        return name;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getFamilyDoctor() {
        return familyDoctor;
    }
    
    
}
